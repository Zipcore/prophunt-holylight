#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>
#include <smlib>
#include <csgocolors>
#include <prophunt>
#include <speedrules>

#define PLUGIN_VERSION "1.0"

public Plugin myinfo = 
{
	name = "Prophunt Holy Light",
	author = ".#Zipcore",
	description = "Holy light grenade for seekers",
	version = PLUGIN_VERSION,
	url = "zipcore.net"
};

#define LoopClients(%1) for(int %1 = 1; %1 <= MaxClients; %1++)

#define LoopIngameClients(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1))

#define LoopIngamePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && !IsFakeClient(%1))

#define LoopAlivePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && IsPlayerAlive(%1))

ConVar g_cvPrice;
ConVar g_cvSort;
ConVar g_cvUnlock;

public void OnPluginStart()
{
	CreateConVar("ph_holylight_version", PLUGIN_VERSION, "Version of this plugin.");
	
	g_cvPrice = CreateConVar("ph_holylight_holy_light_price", "120", "Price for holy light.");
	g_cvSort = CreateConVar("ph_holylight_holy_light_sort", "4", "Shop menu item sorting priority.");
	g_cvUnlock = CreateConVar("ph_holylight_holy_light_unlock", "155", "Shop menu unlock time.");
	
	AutoExecConfig(true, "prophunt-holylight");
	
	HookEvent("flashbang_detonate", OnFlashbangDetonate, EventHookMode_Pre);
	HookEvent("flashbang_detonate", OnFlashbangDetonate);
	HookEvent("player_blind", OnPlayerBlind, EventHookMode_Pre);
	HookEvent("player_blind", OnPlayerBlind);
}

public void OnLibraryAdded(const char[] name)
{
	if(StrEqual(name, "prophunt"))
		PH_RegisterShopItem("Holy Light Grenade", CS_TEAM_CT, g_cvPrice.IntValue, g_cvSort.IntValue, g_cvUnlock.IntValue, false);
}

public void OnMapStart()
{
	PH_RegisterShopItem("Holy Light Grenade", CS_TEAM_CT, g_cvPrice.IntValue, g_cvSort.IntValue, g_cvUnlock.IntValue, false);
}

public Action PH_OnBuyShopItem(int iClient, char[] sName, int &iPoints)
{
	if(StrEqual(sName, "Holy Light Grenade"))
		return Plugin_Handled;
	
	return Plugin_Continue;
}

public void PH_OnBuyShopItemPost(int iClient, char[] sName, int iPoints)
{
	if(StrEqual(sName, "Holy Light Grenade"))
		GivePlayerItem(iClient, "weapon_flashbang");
}

public Action OnPlayerBlind(Event event, const char[] name, bool dontBroadcast)
{
	int iClient = GetClientOfUserId(event.GetInt("userid")); 
	
	SetEntPropFloat(iClient, Prop_Send, "m_flFlashMaxAlpha", 0.5);
	
	if(GetClientTeam(iClient) != CS_TEAM_T)
	{
		SetEntPropFloat(iClient, Prop_Send, "m_flFlashDuration", 0.1);
		return Plugin_Continue;
	}
	
	float fDuration = GetEntPropFloat(iClient, Prop_Send, "m_flFlashDuration", 1);
	
	SpeedRules_ClientAdd(iClient, "holylight", SR_Mul, 0.25, 1.0 + fDuration, 100);
	SpeedRules_ClientAdd(iClient, "holylight", SR_Max, 0.4, 1.0 + fDuration, 100);
	
	return Plugin_Continue;
} 

public Action OnFlashbangDetonate(Event event, const char[] name, bool dontBroadcast)
{
	int iEntity = CreateEntityByName("point_tesla");
	
	if(iEntity != INVALID_ENT_REFERENCE)
	{
		float fPos[3];
		
		fPos[0] = event.GetFloat("x", 0.0);
		fPos[1] = event.GetFloat("y", 0.0);
		fPos[2] = event.GetFloat("z", 0.0);
		
		DispatchKeyValueVector(iEntity, "origin", fPos);
		
		DispatchKeyValue(iEntity, "m_flRadius", "300.0");  
		DispatchKeyValue(iEntity, "m_SoundName", "DoSpark");  
		DispatchKeyValue(iEntity, "beamcount_min", "15");  
		DispatchKeyValue(iEntity, "beamcount_max", "25");
		DispatchKeyValue(iEntity, "texture", "materials/sprites/physbeam.vmt");  
		DispatchKeyValue(iEntity, "m_Color", "255 255 255");  
		DispatchKeyValue(iEntity, "thick_min", "6.0");     
		DispatchKeyValue(iEntity, "thick_max", "9.0");     
		DispatchKeyValue(iEntity, "lifetime_min", "0.3");  
		DispatchKeyValue(iEntity, "lifetime_max", "0.3");  
		DispatchKeyValue(iEntity, "interval_min", "0.1");     
		DispatchKeyValue(iEntity, "interval_max", "0.2");
		
		DispatchSpawn(iEntity);
		
		AcceptEntityInput(iEntity, "TurnOn");     
		AcceptEntityInput(iEntity, "DoSpark");
	
		RemoveEntity(iEntity, 1.0);
	}
}

stock void RemoveEntity(int iEntity, float time = 0.0)
{
	if (time == 0.0)
	{
		if (IsValidEntity(iEntity))
		{
			char edictname[32];
			GetEdictClassname(iEntity, edictname, 32);

			if (!StrEqual(edictname, "player"))
				AcceptEntityInput(iEntity, "kill");
		}
	}
	else if(time > 0.0)
		CreateTimer(time, RemoveEntityTimer, EntIndexToEntRef(iEntity), TIMER_FLAG_NO_MAPCHANGE);
}

public Action RemoveEntityTimer(Handle Timer, any entityRef)
{
	int entity = EntRefToEntIndex(entityRef);
	if (entity != INVALID_ENT_REFERENCE)
		RemoveEntity(entity); // RemoveEntity(...) is capable of handling references
	
	return Plugin_Stop;
}